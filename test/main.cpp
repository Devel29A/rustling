#include <iostream>

#include "CTreeObject.hpp"

int main (int argc, char** argv)
{
   // The very first argument is path to the application. Skip.
   for (int idx = 1; idx < argc; idx++)
   {
      std::cout << "Argument[" << idx << "]\t:" << argv[idx] << std::endl;
   }

   MyTree::CTreeObject root;

   return EXIT_SUCCESS;
}
