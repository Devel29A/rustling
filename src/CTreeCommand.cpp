#include "CTreeCommand.hpp"

namespace MyTree
{
}

namespace std
{
   bool operator<(const MyTree::CTreeCommand & lhs, const MyTree::CTreeCommand & rhs)
   {
      return (lhs.prio() < rhs.prio());
   }
}

