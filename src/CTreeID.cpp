#include "CTreeID.hpp"

namespace MyTree
{
}

// Custom specialization of std::hash
namespace std
{
//------------------------------------------------------------------------------
size_t hash<MyTree::CTreeID>::operator()(const MyTree::CTreeID & id) const
//------------------------------------------------------------------------------
{
   return std::hash<std::size_t>{}(id.get());
}
}
