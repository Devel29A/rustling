#include "CTreeEvent.hpp"

namespace MyTree
{
}

namespace std
{
   bool operator<(const MyTree::CTreeEvent & lhs, const MyTree::CTreeEvent & rhs)
   {
      return (lhs.prio() < rhs.prio());
   }
}

