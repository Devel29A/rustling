#ifndef CTREEOBJECTHPP
#define CTREEOBJECTHPP

#include <queue>
#include <unordered_map>

#include "ITreeObject.hpp"
#include "CTreeCommand.hpp"
#include "CTreeEvent.hpp"
#include "CTreeID.hpp"

namespace MyTree
{
class CTreeObject : public ITreeObject
{
public:
   // General interface
   virtual ITreeResult add       (const ITreeObject  &               );
   virtual ITreeResult remove    (const ITreeObject  &               );
   virtual ITreeResult get       (const ITreeObject  &, ITreeObject &);
   virtual ITreeResult get       (const ITreeID      &, ITreeObject &);
   virtual ITreeResult exec      (const ITreeCommand &               );
   virtual ITreeResult notify    (const ITreeEvent   &               );
   virtual bool        isSame    (const ITreeObject  &               );

   // Operators
   virtual bool operator= (const ITreeObject  & );
   virtual bool operator==(const ITreeObject  & );
   virtual bool operator!=(const ITreeObject  & );

   // Destructor
   virtual ~CTreeObject();

private:
   std::priority_queue<CTreeCommand                > commands;
   std::priority_queue<CTreeEvent                  > events  ;
   std::unordered_map <CTreeID     , CTreeObject * > children;
};
}

#endif // CTREEOBJECTHPP

