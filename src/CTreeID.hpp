#ifndef CTREEIDHPP
#define CTREEIDHPP

#include "ITreeID.hpp"

namespace MyTree
{
class CTreeID : public ITreeID
{
public:
   // Constructors
   CTreeID(const std::size_t & arg) { id = arg; }

   // General interface
   virtual std::size_t get() const { return id; }

private:
   // Hide default constructor
   CTreeID();

   std::size_t id;
};
}
// Custom specialization of std::hash
namespace std
{
template<> struct hash<MyTree::CTreeID>
{
   size_t operator()(const MyTree::CTreeID & id) const;
};
}
#endif // CTREEIDHPP

