#ifndef ITREEEVENTHPP
#define ITREEEVENTHPP

#include <cstddef>

namespace MyTree
{
class ITreeEvent
{
public:
   virtual std::size_t prio() const = 0;

   virtual ~ITreeEvent(){}
};
}

#endif // ITREEEVENTHPP

