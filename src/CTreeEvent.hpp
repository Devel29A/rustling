#ifndef CTREEEVENTHPP
#define CTREEEVENTHPP

#include "ITreeEvent.hpp"

namespace MyTree
{
class CTreeEvent : public ITreeEvent
{
public:
   virtual size_t prio() const { return priority; }

private:
   std::size_t priority;
};
}

namespace std
{
   bool operator<(const MyTree::CTreeEvent & lhs, const MyTree::CTreeEvent & rhs);
}

#endif // CTREEEVENTHPP

