#ifndef ITREEIDHPP
#define ITREEIDHPP

#include <functional>

namespace MyTree
{
class ITreeID
{
public:
   // General interface
   virtual std::size_t get() const = 0;

   // Destructor
   virtual ~ITreeID(){}
};
}

#endif // ITREEIDHPP

