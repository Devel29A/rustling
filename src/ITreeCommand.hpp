#ifndef ITREECOMMANDHPP
#define ITREECOMMANDHPP

#include <cstddef>

namespace MyTree
{
class ITreeCommand
{
public:
   virtual std::size_t prio() const = 0;

   virtual ~ITreeCommand(){}
};
}

#endif // ITREECOMMANDHPP

