#include "CTreeObject.hpp"
#include "CTreeResult.hpp"

namespace MyTree
{
//------------------------------------------------------------------------------
CTreeObject::~CTreeObject()
//------------------------------------------------------------------------------
{
}

//------------------------------------------------------------------------------
ITreeResult CTreeObject::add(const ITreeObject & arg)
//------------------------------------------------------------------------------
{
   CTreeResult result;
   return result;
}

//------------------------------------------------------------------------------
ITreeResult CTreeObject::remove(const ITreeObject & arg)
//------------------------------------------------------------------------------
{
   CTreeResult result;
   return result;
}

//------------------------------------------------------------------------------
ITreeResult CTreeObject::get(const ITreeObject & arg, ITreeObject & obj)
//------------------------------------------------------------------------------
{
   CTreeResult result;
   return result;
}

//------------------------------------------------------------------------------
ITreeResult CTreeObject::get(const ITreeID & arg, ITreeObject & obj)
//------------------------------------------------------------------------------
{
   CTreeResult result;
   return result;
}

//------------------------------------------------------------------------------
ITreeResult CTreeObject::exec(const ITreeCommand & arg)
//------------------------------------------------------------------------------
{
   CTreeResult result;
   return result;
}

//------------------------------------------------------------------------------
ITreeResult CTreeObject::notify(const ITreeEvent & arg)
//------------------------------------------------------------------------------
{
   CTreeResult result;
   return result;
}

//------------------------------------------------------------------------------
bool CTreeObject::isSame(const ITreeObject & arg)
//------------------------------------------------------------------------------
{
   bool result;
   return result;
}

//------------------------------------------------------------------------------
bool CTreeObject::operator= (const ITreeObject & arg)
//------------------------------------------------------------------------------
{
   bool result = false;
   return result;
}

//------------------------------------------------------------------------------
bool CTreeObject::operator==(const ITreeObject & arg)
//------------------------------------------------------------------------------
{
   bool result = false;
   return result;
}

//------------------------------------------------------------------------------
bool CTreeObject::operator!=(const ITreeObject & arg)
//------------------------------------------------------------------------------
{
   bool result = false;
   return result;
}

}
