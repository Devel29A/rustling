#ifndef CTREECOMMANDHPP
#define CTREECOMMANDHPP

#include "ITreeCommand.hpp"

namespace MyTree
{
class CTreeCommand : public ITreeCommand
{
public:
   virtual size_t prio() const { return priority; }

private:
   std::size_t priority;
};
}

namespace std
{
   bool operator<(const MyTree::CTreeCommand & lhs, const MyTree::CTreeCommand & rhs);
}

#endif // CTREECOMMANDHPP

