#ifndef ITREEOBJECTHPP
#define ITREEOBJECTHPP

#include "ITreeCommand.hpp"
#include "ITreeEvent.hpp"
#include "ITreeResult.hpp"
#include "ITreeID.hpp"

namespace MyTree
{
class ITreeObject
{
public:
   // General interface
   virtual ITreeResult add       (const ITreeObject  &               ) = 0;
   virtual ITreeResult remove    (const ITreeObject  &               ) = 0;
   virtual ITreeResult get       (const ITreeObject  &, ITreeObject &) = 0;
   virtual ITreeResult get       (const ITreeID      &, ITreeObject &) = 0;
   virtual ITreeResult exec      (const ITreeCommand &               ) = 0;
   virtual ITreeResult notify    (const ITreeEvent   &               ) = 0;
   virtual bool        isSame    (const ITreeObject  &               ) = 0;

   // Operators
   virtual bool operator= (const ITreeObject  & ) = 0;
   virtual bool operator==(const ITreeObject  & ) = 0;
   virtual bool operator!=(const ITreeObject  & ) = 0;

   // Destructor
   virtual ~ITreeObject() { }
};
}

#endif // ITREEOBJECTHPP

